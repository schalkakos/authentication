<?php 

class Db {
  private $db_connection;
  public $error = false;
  public $result;

  public function __construct() {
    $config = $GLOBALS['mysql_config'];
    try {
      $this->db_connection = new PDO('mysql:host=' . $config['host'] . ';dbname=' . $config['db'], $config['username'], $config['password']);
    }catch (Exception $e) {
      echo 'Caught exception: ',  $e->getMessage(), "\n";
    }
  }

  public function get($table, $where = null){
    $stmt = "SELECT * FROM {$table} WHERE {$where}";
    $query = $this->db_connection->query($stmt);

    if($query->execute()) {
      $result = $query->fetchAll(PDO::FETCH_ASSOC);
      if($result) {
        $this->result = $result;
      }else {
        $this->error = true;
      }
    }else {
      $this->error = true;
    }
    return $this;
  }

  public function create($table, $insert_values) {
    $column_names = array();
    $placeholders = array();
    foreach($insert_values as $key => $value) {
      $column_names[] = $key;
      $values[":{$key}"] = "{$value}";
      $placeholders[] = ":{$key}";
    }

    $sql = "INSERT INTO {$table} (".implode(", ", $column_names).") VALUES (".implode(", ", $placeholders).")";

    $statement = $this->db_connection->prepare($sql);
    foreach($values as $placeholder => $value){
      $statement->bindValue($placeholder, $value);
    }
    return $statement->execute();
  }
}