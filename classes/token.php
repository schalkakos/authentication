<?php

class Token {
  public static function generate_token() {
    if(!Session::exists("csrf_token")) {
      $token = bin2hex(random_bytes(32));
      Session::create("csrf_token", $token);
    }else  {
      $token = Session::get("csrf_token");
    }
    return $token;
  }

  public static function validateToken($form_token, $session_token) {
    if($form_token != $session_token) {
      return false;
    }
    return true;
  }
}