<?php

class User {
  public function login($username, $password) {
    $db = new Db();
    $user = $db->get("users", "username='{$username}'");
    if($user->result) {
      $hashed_password = $user->result[0]['password'];
      if(Hash::verifyPassword($password, $user->result[0]["password"]) && !$user->error){
        Session::create("user", $user->result[0]['id']);
        return true;
      };
    }

    return false;
  }
  
  public function register($credentials){
    $credentials["password"] = Hash::hashPassword($credentials['password']);
    $db = new DB();
    $db->create("users", $credentials);
  }

  public function logout(){
    Session::delete("user");
  }

  public function find($user = null) {
    if($user){
      $where = is_numeric($user) ? "id='{$user}'" : "username='{$user}'";
      $db = new Db();
      $user = $db->get("users", $where);

      if($user->result) {
        return true;
      }
    };
    return false;
  }
}