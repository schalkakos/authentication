<?php

class Validate {
  public $error;
  public $passed = false;

  public function valid($credentials, $rules) {
    foreach($rules as $input => $rule_array) {
      foreach($rule_array as $rule => $rule_value) {
        $field_value = $credentials[$input];

        switch($rule) {
          case 'required':
            if(empty($field_value)) {
              $this->error = "{$input} is required";
              break 3;
            }
            break;
          case "min":
            if(strlen($field_value) < $rule_value) {
              $this->error = "{$input} must be {$rule_value} characters long";
              break 3;
            }
            break;
          case "unique":
            $db = new DB();
            $user = $db->get("users", "{$input}='{$field_value}'");
            if(!$user->error) {
              $this->error = "{$input} already exists";
              break 3;
            }
            break;
          case "matches":
            if($credentials[$rule_value] !== $field_value){
              $this->error = "{$rule_value} and " . str_replace("_", " ", $input) . " must match";
              break 3;
            }
            break;
        }
      }
    }
    if(!$this->error){
      $this->passed = true;
    }
    return $this;
  }
}