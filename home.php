<?php
  require_once('./init.php');
  if(!Authenticate::isLoggedIn()) {
    header('Location: ./login.php');
  }

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
  <link href="./styles/header.css" type="text/css" rel="stylesheet">
  <link href="./styles/home.css" type="text/css" rel="stylesheet">
  <title>Home</title>
</head>
<body>
  <?php include("./includes/header.php") ?>
  <div class="content">
    <p class="content__placeholder">this is the home page that can only be visited after login</p>
  </div>
</body>
</html>