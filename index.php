<?php 
  require_once('./init.php');
  if(Authenticate::isLoggedIn()) {
    header("Location: ./home.php");
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap" rel="stylesheet">
  <link href="./styles/index.css" type="text/css" rel="stylesheet">
  <link href="./styles/header.css" type="text/css" rel="stylesheet">
  <title>Landing page</title>
</head>
<body>
  <?php include("./includes/header.php") ?>
  <div class="content">
    <p class="content__intro">Here we'll tell you about how much of a life changing experience it is to use our completly made up website!</p>
    <a class="content__sign-up" href="./register.php">
      <span>Sign Up</span>
    </a>
  </div>
</body>
</html>