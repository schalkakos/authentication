<?php
session_start();
define('DS', DIRECTORY_SEPARATOR);
define('ROOT', dirname(__FILE__));

$GLOBALS['mysql_config'] = array(
  'host' => 'localhost',
  'username' => 'root',
  'password' => '',
  'db' => 'Prioris'
);

spl_autoload_register(function($class){
  if(file_exists(ROOT . DS ."classes" . DS . $class . ".php")){
    require_once(ROOT . DS ."classes" . DS . $class . ".php");
  } else if(file_exists(ROOT . DS ."middlewares" . DS . $class . ".php")) {
    require_once(ROOT . DS ."middlewares" . DS . $class . ".php");
  }
});