<?php
  require_once('./init.php');

  $error = "";
  if(isset($_POST['username']) && isset($_POST['password'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];
    $user = new User();
        
    

    if($user->login($username, $password) && Token::validateToken($_POST['csrf_token'], Session::get("csrf_token"))) {
    header('Location: ./index.php');
    }else{
      $error = "Login failed wrong username or password";
    }
  }else if(Authenticate::isLoggedIn()) {
    header('Location: ./home.php');
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
  <link href="./styles/login.css" type="text/css" rel="stylesheet" >
  <title>Login</title>
</head>
<body>
<div class="content">
  <h3 class="content__title">Sign in</h3>
  <form class="form" action="" method="post">
    <span class="form__error" ><?php echo $error;?></span>
    <input class="form__input" type="text" name="username" placeholder="Username">
    <input class="form__input" type="password" name="password" placeholder="Password">
    <input type="hidden" name="csrf_token" value="<?php echo Token::generate_token() ?>">
    <div class="form__actions">
      <button class="form__submit" type="submit">Sign in</button>
      <a class="form__sign-up" href="./register.php">Create an account</a>
    </div>
  </form>
</div>

</body>
</html>