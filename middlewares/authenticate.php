<?php
 
class Authenticate {
  public static function isLoggedin() {
    if(Session::exists("user")) {
      $session_id = Session::get("user");
      $user = new User();
      if(!$user->find($session_id)) {
        return false;
      }
      return true;
    }
    return false;
  }
}