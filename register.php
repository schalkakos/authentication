<?php
  require_once('./init.php'); 

  $token = Token::generate_token();
  $error = "";
  if(isset($_POST["username"])) {

    $validate = new Validate();
    $validation = $validate->valid($_POST, array(
      "username" => array(
        "required" => true,
        "unique" => true,
      ),
      "password" => array(
        "required" => true,
        "min" => 4,
      ),
      "confirm_password" => array(
        "required" => true,
        "matches" => "password",
      ))
      );
      if($validation->passed && Token::validateToken($_POST["csrf_token"], Session::get("csrf_token"))){
        $user = new User();
        $credentials = array(
          "username" => $_POST['username'],
          "password" => $_POST['password'],
        );
        
        $user->register($credentials);
        header('Location: ./login.php');
      } else if($validation->error) {
        $error = $validation->error;
      }
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
  <link href="./styles/register.css" type="text/css" rel="stylesheet" >
  <title>Sign Up</title>
</head>
<body>
<div class="content">
  <h3 class="content__title">Sign up</h3>
  <form class="form" action="" method="post">
    <div class="form__error">
      <span ><?php echo $error?></span>  
    </div>
    <input class="form__input" type="text" name="username" placeholder="Username">
    <input class="form__input" type="password" name="password" placeholder="Password">
    <input class="form__input" type="password" name="confirm_password" placeholder="Confirm password">
    <input type="hidden" name="csrf_token" value="<?php echo  $token;?>">
    <div class="form__actions">
      <button class="form__submit" type="submit">Sign Up</button>
      <a class="form__login" href="./login.php">Sign In</a>
    </div>
    
  </form>
  </div>
</body>
</html>